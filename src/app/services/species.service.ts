import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { species } from '../model/species';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class SpeciesService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<species[]> {
    return this.httpClient.get<species[]>(
              environment.backendUrl + 'species/');
  }

  findById(id:string) : Observable<species>{
    console.log(environment.backendUrl + id + '/');
    return this.httpClient.get<species>(
      environment.backendUrl + 'species/' + id + '/');
  }
}