import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { vehicles } from '../model/vehicles';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<vehicles[]> {
    return this.httpClient.get<vehicles[]>(
              environment.backendUrl);
  }

  findById(id:string) : Observable<vehicles>{
    console.log(environment.backendUrl + id + '/');
    return this.httpClient.get<vehicles>(
      environment.backendUrl + id + '/');
  }
}