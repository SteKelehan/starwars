import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { films } from '../model/films';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<films[]> {
    return this.httpClient.get<films[]>(
              environment.backendUrl  + 'films/');
  }

  findById(id:string) : Observable<films>{
    return this.httpClient.get<films>(
      environment.backendUrl + 'films/' + id + '/');
  }
}