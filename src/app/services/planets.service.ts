import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { planets } from '../model/planets';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<planets[]> {
    return this.httpClient.get<planets[]>(
              environment.backendUrl + 'planets/');
  }

  findById(id:string) : Observable<planets>{
    console.log(environment.backendUrl + id + '/');
    return this.httpClient.get<planets>(
      environment.backendUrl + 'planets/' + id + '/');
  }
}