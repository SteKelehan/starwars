import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { spaceShip } from '../model/spaceShip';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class SpaceShipService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<spaceShip[]> {
    return this.httpClient.get<spaceShip[]>(
              environment.backendUrl + 'starships/');
  }

  findById(id:string) : Observable<spaceShip>{
    return this.httpClient.get<spaceShip>(
      environment.backendUrl + 'starships/' + id + '/');
  }
}
