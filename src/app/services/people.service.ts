import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { people } from '../model/people';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { strictEqual } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private httpClient: HttpClient) { }

  findAll(): Observable<people[]> {
    return this.httpClient.get<people[]>(
              environment.backendUrl);
  }

  findById(id:string) : Observable<people>{
    console.log(environment.backendUrl + 'people/' + id + '/');
    return this.httpClient.get<people>(
      environment.backendUrl + 'people/' + id + '/');
  }
}