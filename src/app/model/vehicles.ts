// {
//     "cargo_capacity": "50000",
//     "consumables": "2 months",
//     "cost_in_credits": "150000",
//     "created": "2014-12-10T15:36:25.724000Z",
//     "crew": "46",
//     "edited": "2014-12-10T15:36:25.724000Z",
//     "length": "36.8",
//     "manufacturer": "Corellia Mining Corporation",
//     "max_atmosphering_speed": "30",
//     "model": "Digger Crawler",
//     "name": "Sand Crawler",
//     "passengers": "30",
//     "pilots": [],
//     "films": [
//         "https://swapi.co/api/films/1/"
//     ],
//     "url": "https://swapi.co/api/vehicles/4/",
//     "vehicle_class": "wheeled"
// }

export class vehicles {
    constructor(public cargo_capacity: number,
                public consumables: string,
                public cost_in_credits: 150000,
                public created: string,
                public crew: number,
                public edited: string,
                public length: number,
                public manufacturer: string,
                public max_atmosphering_speed: number, 
                public model: string, 
                public name: string,
                public passengers: number,
                public pilots: string[], 
                public films: string[],
                public vehicle_class: string,
                public url: string
                ) {}
}