// "climate": "Arid",
// "diameter": "10465",
// "gravity": "1 standard",
// "name": "Tatooine",
// "orbital_period": "304",
// "population": "200000",
// "residents": [
//     "https://swapi.co/api/people/1/",
//     "https://swapi.co/api/people/2/",
//     ...
// ],
// "rotation_period": "23",
// "surface_water": "1",
// "terrain": "Dessert",
// "url": "https://swapi.co/api/planets/1/"
// }

export class films {
    constructor(public climate: string,
                public diameter: number,
                public gravity: string,
                public name: string,
                public population: number,
                public orbital_period: number,
                public residents: string[],
                public rotation_period: number,
                public surface_water: number, 
                public terrain: string, 
                public url: string,
                ) {}
}