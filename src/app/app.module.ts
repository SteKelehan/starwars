import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SpaceShipTableComponent } from './spaceShip-table/spaceShip-table.component';
import { FilmTableComponent } from './film-table/film-table.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { PlanetsTableComponent } from './planets-table/planets-table.component';
import { SpeciesTableComponent } from './species-table/species-table.component';
import { VehivlesTableComponent } from './vehivles-table/vehivles-table.component';

@NgModule({
  declarations: [
    AppComponent,
    SpaceShipTableComponent,
    FilmTableComponent,
    PeopleTableComponent,
    PlanetsTableComponent,
    SpeciesTableComponent,
    VehivlesTableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {



 }



