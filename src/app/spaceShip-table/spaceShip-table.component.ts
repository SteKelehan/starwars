import { Component, OnInit } from '@angular/core';
import { SpaceShipService } from '../services/spaceShip.service';
import { spaceShip } from '../model/spaceShip';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-spaceShip-table',
  templateUrl: './spaceShip-table.component.html',
  styleUrls: ['./spaceShip-table.component.css']
})
export class SpaceShipTableComponent implements OnInit {

  spaceships: spaceShip[];
 // spaceShip: spaceShip;

  constructor(private spaceShipService: SpaceShipService) {
  }

  ngOnInit() {
    this.spaceShipService.findAll().subscribe(
      spaceShipsArray => {
          this.spaceships = spaceShipsArray.results;
          console.log(this.spaceships);
      },
      error => {
        console.log(error);
      }
    );
    
    // this.spaceShipService.findById('1').subscribe(
    //   // tslint:disable-next-line: no-shadowed-variable
    //   spaceShip => {
    //     console.log(spaceShip);
    //     this.spaceships = [spaceShip];
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
  }
}
