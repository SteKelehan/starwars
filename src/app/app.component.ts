import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  title = 'angular-employees-exercise';

  SpaceShips: boolean = true;
  People: boolean = false;
  Planets: boolean = false;
  Films: boolean = false;
  Species: boolean = false;
  Vehicles: boolean = false;

  switch(choice: string){
    switch(choice) {
      case 'SpaceShips':
        this.SpaceShips = !this.SpaceShips;
        break;
      case 'People':
        this.People = !this.People;
        break;
      case 'Planets':
        this.Planets = !this.Planets;
        break;
      case 'Films':
        this.Films = !this.Films;
        break;
      case 'Species':
        this.Species = !this.Species;
        break;
      case 'Vehicles':
        this.Vehicles = !this.Vehicles;
        break;
    }
  }

 
}


