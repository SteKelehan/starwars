import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehivlesTableComponent } from './vehivles-table.component';

describe('VehivlesTableComponent', () => {
  let component: VehivlesTableComponent;
  let fixture: ComponentFixture<VehivlesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehivlesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehivlesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
